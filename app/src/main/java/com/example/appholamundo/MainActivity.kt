package com.example.appholamundo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import org.w3c.dom.Text

class MainActivity : AppCompatActivity() {
    private lateinit var lblSaludo : TextView
    private lateinit var txtNombre : EditText
    private lateinit var btnSaludar : Button
    private lateinit var btnLimpiar : Button
    private lateinit var btnCerrar : Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        eventosButones()
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    fun iniciarComponentes(){
        lblSaludo = findViewById(R.id.lblSaludar) as TextView
        txtNombre = findViewById(R.id.txtNombre) as EditText
        btnSaludar = findViewById(R.id.btnSaludo) as Button
        btnLimpiar = findViewById(R.id.btnLimpiar) as Button
        btnCerrar = findViewById(R.id.btnCerrar) as Button

    }
    fun eventosButones(){
        btnCerrar.setOnClickListener(View.OnClickListener {
            //Codificar
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Hola Mundo")
            builder.setMessage("¿Deséas Salir?")

            builder.setPositiveButton("Si"){
                dialog, which -> finish()
            }
            builder.setNeutralButton("No"){
                dialog, which ->
                Toast.makeText(applicationContext, "Cancelado", Toast.LENGTH_SHORT).show()
            }
builder.show()
        })
        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtNombre.setText("")
            lblSaludo.setText("")
        })//Termina btnLimpiar
        btnSaludar.setOnClickListener(View.OnClickListener {
            var strNombre : String = ""

            if(txtNombre.text.toString().contentEquals("")) {
                Toast.makeText(applicationContext, "Falto capturar Nombre", Toast.LENGTH_SHORT)
                    .show()
            }else {
                strNombre = "Hola " + txtNombre.text.toString() + ", ¿Cómo estás?"
                lblSaludo.text = strNombre
            }
        })//termina btn Saludar
    }//Termina evento botones

}//Termina la clase