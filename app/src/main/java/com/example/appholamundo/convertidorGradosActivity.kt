package com.example.appholamundo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.activity.enableEdgeToEdge

class convertidorGradosActivity : AppCompatActivity() {

    private lateinit var txtCantidad: EditText
    private lateinit var txtResultado: TextView
    private lateinit var rdbCel: RadioButton
    private lateinit var rdbFar: RadioButton
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_convertidor_grados)
        iniciarComponentes()
        eventosClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun iniciarComponentes() {
        txtCantidad = findViewById(R.id.txtCantidad)
        txtResultado = findViewById(R.id.txtResultado)
        rdbCel = findViewById(R.id.rdbCel)
        rdbFar = findViewById(R.id.rdbFar)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)
    }

    private fun eventosClic() {
        btnCalcular.setOnClickListener {
            val cantidadStr = txtCantidad.text.toString()
            if (cantidadStr.isEmpty()) {
                Toast.makeText(this, "Faltó capturar Cantidad", Toast.LENGTH_SHORT).show()
            } else {
                val cantidad = cantidadStr.toFloat()
                if (rdbCel.isChecked) {
                    val fahrenheit = (cantidad * 9 / 5) + 32
                    txtResultado.text = String.format("%.2f", fahrenheit)
                } else if (rdbFar.isChecked) {
                    val celsius = (cantidad - 32) * 5 / 9
                    txtResultado.text = String.format("%.2f", celsius)
                }
            }
        }

        btnLimpiar.setOnClickListener {
            txtCantidad.text.clear()
            txtResultado.text = ""
        }

        btnCerrar.setOnClickListener {
            finish()
        }
    }
}
