package com.example.appholamundo

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class menuActivity : AppCompatActivity() {
    private lateinit var crvHola: CardView
    private lateinit var crvImc: CardView
    private lateinit var crvGrados: CardView
    private lateinit var crvMoneda: CardView
    private lateinit var crvCotizacion: CardView
    private lateinit var crvSpinner: CardView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        // Llamar Funciones
        IniciarComponentes()
        eventosClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }

    private fun IniciarComponentes() {
        crvHola = findViewById(R.id.crvHola)
        crvImc = findViewById(R.id.crvImc)
        crvGrados = findViewById(R.id.crvGrados)
        crvMoneda = findViewById(R.id.crvMonedas)
        crvCotizacion = findViewById(R.id.crvCotizacion)
    }

    private fun eventosClic() {
        crvHola.setOnClickListener {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
        }
        crvImc.setOnClickListener {
            try {
                val intent = Intent(this, imcActivity::class.java)
                startActivity(intent)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        crvGrados.setOnClickListener {
            val intent = Intent(this, convertidorGradosActivity::class.java)
            startActivity(intent)
        }
        // Añadir listeners para otros CardView si es necesario
    }
}
