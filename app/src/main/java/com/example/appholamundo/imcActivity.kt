package com.example.appholamundo

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class imcActivity : AppCompatActivity() {

    private lateinit var lblResultado: TextView
    private lateinit var txtAltura: EditText
    private lateinit var txtPeso: EditText
    private lateinit var btnCalcular: Button
    private lateinit var btnLimpiar: Button
    private lateinit var btnCerrar: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_imc) // Asegúrate que este es el nombre correcto de tu XML

        iniciarComponentes()
        configurarEventosBotones()
    }

    private fun iniciarComponentes() {
        lblResultado = findViewById(R.id.lblResultado)
        txtAltura = findViewById(R.id.txtAltura)
        txtPeso = findViewById(R.id.txtPeso)
        btnCalcular = findViewById(R.id.btnCalcular)
        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        // Cambiar el color del texto a rojo
        lblResultado.setTextColor(ContextCompat.getColor(this, android.R.color.holo_red_dark))
        lblResultado.text = "El resultado aparecerá aquí:"
    }

    private fun configurarEventosBotones() {
        btnCalcular.setOnClickListener {
            val alturaStr = txtAltura.text.toString()
            val pesoStr = txtPeso.text.toString()

            if (alturaStr.isEmpty() || pesoStr.isEmpty()) {
                Toast.makeText(applicationContext, "Por favor, ingresa altura y peso", Toast.LENGTH_SHORT).show()
            } else {
                val altura = alturaStr.toFloat() / 100 // Convertir a metros
                val peso = pesoStr.toFloat()

                if (altura <= 0 || peso <= 0) {
                    Toast.makeText(applicationContext, "Altura y peso deben ser mayores que 0", Toast.LENGTH_SHORT).show()
                } else {
                    val imc = peso / (altura * altura)
                    lblResultado.text = "Tu IMC es: %.2f".format(imc)
                }
            }
        }

        btnLimpiar.setOnClickListener {
            txtAltura.setText("")
            txtPeso.setText("")
            lblResultado.text = "El resultado aparecerá aquí:"
        }

        btnCerrar.setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Cerrar")
            builder.setMessage("¿Deseas salir?")

            builder.setPositiveButton("Sí") { _, _ -> finish() }
            builder.setNeutralButton("No") { _, _ ->
                Toast.makeText(applicationContext, "Cancelado", Toast.LENGTH_SHORT).show()
            }
            builder.show()
        }
    }
}
